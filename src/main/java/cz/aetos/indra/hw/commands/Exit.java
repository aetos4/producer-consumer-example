package cz.aetos.indra.hw.commands;

import cz.aetos.indra.hw.DBUtil;
import cz.aetos.indra.hw.commands.util.CommandDB;
import cz.aetos.indra.hw.commands.util.CommandDBResult;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.SQLException;

@Slf4j
public class Exit extends CommandDB {

    @Override
    public CommandDBResult execute(Connection conn) {
        log.info("start");
        CommandDBResult result = new CommandDBResult(); // FIXME null object
        try {
            conn.close();
            result.setResult(14);
        } catch (SQLException e) {
            log.error("Error while closing connection, {}", e);
            result.setResult(-14);
            throw new RuntimeException(e);
        } finally {
            log.info("end with result {}", result.getResult());
            return result; // FIXME null object
        }
    }
}
