#!/bin/bash

find . -type d -iname target -print0 |  xargs -0 -I {} /bin/rm -rf "{}"
find . -type d -iname db_dev -print0 |  xargs -0 -I {} /bin/rm -rf "{}"

mvn \
  package -Dmaven.test.skip=true && \
  java -jar target/user-service-0.1.0-SNAPSHOT-jar-with-dependencies.jar \
  "$@"