package cz.aetos.indra.hw;

import cz.aetos.indra.hw.commands.Add;
import cz.aetos.indra.hw.commands.DeleteAll;
import cz.aetos.indra.hw.commands.Exit;
import cz.aetos.indra.hw.commands.PrintAll;
import cz.aetos.indra.hw.commands.util.CommandDB;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

@Slf4j
public class Producer implements Runnable {

    private final ArrayBlockingQueue queue;
    private List<CommandDB> batch;

    public Producer(ArrayBlockingQueue queue) {
        this.queue = queue;
        this.batch = new ArrayList<>();
    }

    public Producer(ArrayBlockingQueue queue, List<CommandDB> batch) {
        this.queue = queue;
        this.batch = batch;
    }

    @Override
    public void run() {
        log.info("Producer start");

        boolean run = true;
        int counter = 0; // NOTE: this value is only for logging purpose
        while(run) {
            produce(batch);
            // finish producer work
            run = false;
            log.info("Counter: " + ++counter);
        }

        log.info("Producer stop");
    }

    private void produce(List<CommandDB> toProcessing) {
        for (int i = 0; i < toProcessing.size(); i++) {
            CommandDB command = toProcessing.get(i);
            log.info("OFFER " + command.getClass());
            queue.offer(command);
        }
    }

}
