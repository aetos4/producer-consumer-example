package cz.aetos.indra.hw;


import cz.aetos.indra.hw.commands.Add;
import cz.aetos.indra.hw.commands.DeleteAll;
import cz.aetos.indra.hw.commands.Exit;
import cz.aetos.indra.hw.commands.PrintAll;
import cz.aetos.indra.hw.commands.util.CommandDB;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

@Slf4j
public class ProducerConsumerDemonstrationTest {

    private Connection conn;
    public static final String DB_FOLDER_TEST = "db_test";

    private List<CommandDB> firstBatch = List.of(
            new Add(new UserDTO(1, "a1", "Robert")),
            new Add(new UserDTO(2, "a2", "Martin")),
            new PrintAll()
    );

    private List<CommandDB> secondBatch = List.of(
            new Add(new UserDTO(3, "b3", "Patrik")),
            new Add(new UserDTO(4, "b4", "Standa")),
            new PrintAll(),
            new DeleteAll(),
            new PrintAll(),
            new Exit()
    );

    @BeforeEach
    public void setUp() {
        log.info("start");
        conn = DBUtil.getOrCreateConnection(DB_FOLDER_TEST);
        DBUtil.setupDB(conn);
    }

    @AfterEach
    public void closeConnection() {
        try {
            conn.close();
        } catch (SQLException e) {
            log.error("Error during close connection, {}", e);
            throw new RuntimeException(e);
        }
    }

    @Test
    public void producerAloneTest() {
        // SETUP
        ArrayBlockingQueue<CommandDB> queue = new ArrayBlockingQueue<>(100);
        Thread t1 = new Thread(new Producer(queue, firstBatch), "t1");
        t1.start();
        System.out.println(t1.isAlive());
        Thread t2 = new Thread(new Producer(queue, secondBatch), "t2");
        t2.start();

        // TEST
        await().atMost(5, TimeUnit.SECONDS).until(() -> !(t1.isAlive() || t2.isAlive()));
        Assertions.assertEquals(firstBatch.size()+secondBatch.size(), queue.size(), "Producer add all commands into queue");
    }

    @Test
    public void consumerAloneTest() {
        // SETUP
        ArrayBlockingQueue<CommandDB> queue = new ArrayBlockingQueue<>(10);
        Thread t1 = new Thread(new Consumer(queue, conn), "t1");
        t1.start();

        Thread t2 = new Thread(() -> queue.offer(new Exit()));
        t2.start();

        // TEST
        await().atMost(1500, TimeUnit.MILLISECONDS).until(() -> !(t2.isAlive() || t2.isAlive()));
        Assertions.assertEquals(false, t1.isAlive(), "Consumer finishes its work when receives exit command.");
        Assertions.assertEquals(0, queue.size(), "Queue is empty.");
    }

}
