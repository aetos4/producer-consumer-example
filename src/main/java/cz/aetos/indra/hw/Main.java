package cz.aetos.indra.hw;

import cz.aetos.indra.hw.commands.Add;
import cz.aetos.indra.hw.commands.DeleteAll;
import cz.aetos.indra.hw.commands.Exit;
import cz.aetos.indra.hw.commands.PrintAll;
import cz.aetos.indra.hw.commands.util.CommandDB;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;

@Slf4j
public class Main {

    private static List<CommandDB> batch = List.of(
            new Add(new UserDTO(1, "a1", "Robert")),
            new Add(new UserDTO(2, "a2", "Martin")),
            new PrintAll(),
            new DeleteAll(),
            new PrintAll(),
            new Exit()
    );

    public static void main(String[] args) {
        log.info("Main method start");

        Connection conn = DBUtil.getOrCreateConnection(DBUtil.DB_FOLDER_DEV);
        DBUtil.setupDB(conn);

        ArrayBlockingQueue queue = new ArrayBlockingQueue(10);
        Producer producer = new Producer(queue, batch);
        Consumer consumer = new Consumer(queue, conn);

        Thread t1 = new Thread(producer, "Thread 1");
        Thread t2 = new Thread(consumer, "Thread 2");
        t1.start();
        t2.start();

        log.info("Main method end");
    }

}