package cz.aetos.indra.hw.commands;

import cz.aetos.indra.hw.commands.util.CommandDBResult;
import cz.aetos.indra.hw.commands.util.CommandTable;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;

@Slf4j
public class DropTable extends CommandTable {

    public DropTable(String tableName) {
        super(tableName, "drop table $T");
    }

    @Override
    public CommandDBResult execute(Connection conn) {
        log.info("start, table = {} (sql: {} )", tableName, sqlTemplate);
        String sql = sqlTemplate.replace("$T", tableName);
        CommandDBResult result = executeInternal(conn, true, sql);
        log.info("end");
        return result;
    }


}
