package cz.aetos.indra.hw.commands;

import cz.aetos.indra.hw.UserDTO;
import cz.aetos.indra.hw.commands.util.CommandDB;
import cz.aetos.indra.hw.commands.util.CommandDBResult;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

@Slf4j
public class PrintAll extends CommandDB {

    public PrintAll() {
        super("select * from SUSERS");
    }

    @Override
    public CommandDBResult execute(Connection conn) {
        log.info("start ( {} )", sqlTemplate);

        // get data form db
        CommandDBResult result = executeInternal(conn, false, sqlTemplate);
        // print
        for (UserDTO userDTO: result.getUsers()) {
            log.info("User({}, {}, {})",
                    userDTO.id(),
                    userDTO.guid(),
                    userDTO.name());
        }

        log.info("end");
        return result;
    }

}
