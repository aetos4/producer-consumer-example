package cz.aetos.indra.hw.commands.util;

import cz.aetos.indra.hw.UserDTO;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.apache.derby.impl.jdbc.EmbedResultSet;

import java.sql.ResultSet;
import java.util.List;
import java.util.Optional;

@Getter
@Setter
public class CommandDBResult {

    private Integer result;

    private List<UserDTO> users;

}
