package cz.aetos.indra.hw;


public record UserDTO(Integer id, String guid, String name) {
}

