package cz.aetos.indra.hw;

import cz.aetos.indra.hw.commands.DropTable;
import cz.aetos.indra.hw.commands.RunSQL;
import cz.aetos.indra.hw.commands.util.CommandDB;
import cz.aetos.indra.hw.commands.util.CommandDBResult;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.derby.client.net.NetConnection;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class DBUtil {

    private static DBUtilConfig config = new DBUtilConfig(DBUtilConfig.Mode.DEFAULT);

    static {
        // FIXME read from args[]
        config.mode = DBUtilConfig.Mode.CUSTOM;
    }

    public static final String DB_FOLDER_DEV = "db_dev";

    private static Connection conn;

    public static synchronized Connection getOrCreateConnection(String folder) {
        // checks whether connection is created
        try {
            if (conn != null && !conn.isClosed()) {
                return conn;
            }
        } catch (SQLException e) {
            log.error("Error occurred while checking connection");
            throw new RuntimeException(e);
        }


        log.info("going to create connection");
        // load driver
        try {
            Class.forName("org.apache.derby.jdbc.EmbeddedDriver");
        } catch (ClassNotFoundException e) {
            log.error("Error occurred while loading db driver class, {}", e);
            throw new RuntimeException(e);
        }

        // connection string
        String urlConnection = "jdbc:derby:$FOLDER;create=true"
                .replace("$FOLDER", folder.toString());

        // get connection
        try {
            conn = DriverManager.getConnection(urlConnection);
        } catch (SQLException e) {
            log.error("Error occurred while creating connection, {}", e);
            throw new RuntimeException(e);
        }

        return conn;
    }

    public static synchronized void closeConnection() {
        try {
            close:
            if (conn != null && !conn.isClosed()) {
                log.info("going to close connection");
                conn.close();
                if (conn.isClosed()) {
                    conn = null;
                } else {
                    break close;
                }
            }
        } catch (SQLException e) {
            log.error("Error during close connection, {}", e);
            throw new RuntimeException(e);
        }
    }

    public static synchronized CommandDBResult executeSql(Connection conn, boolean update, String sql) {
        log.info("command {} going to run sql = {}",  DBUtil.class.getClass(), sql);
        // create or defined objects
        CommandDBResult result = new CommandDBResult();
        Statement s = null;
        ResultSet rs = null;

        // preventively sets unsuccessfully result code
        result.setResult(-13);

        try {
            // re-configure connection
            config.mode.configure(conn);
            // create statement
            s = conn.createStatement();
            conn.setSavepoint();
            // updates or reads
            if (update) {
                s.executeUpdate(sql);
            } else {
                rs = s.executeQuery(sql);
                // convert result
                List<UserDTO> users = new ArrayList<>();
                while (rs.next()) {
                    users.add(new UserDTO(
                            rs.getInt(1),
                            rs.getString(2),
                            rs.getString(3)));
                }
                result.setUsers(users);
            }

            // commit and set successful result
            conn.commit();
            result.setResult(0);
        } catch (SQLException e) {
            try {
                conn.rollback();
            } catch (SQLException ex) {
                log.error("Error occurred while rollback was processing, {}", e);
                throw new RuntimeException(ex);
            }
            result.setResult(-14);
            log.error("SQL exception occurred while work with database ({}), {}", result.getResult(),  e);
            throw new RuntimeException(e);
        } finally {
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException e) {
                    result.setResult(-15);
                    log.error("Error occurred while closing db statement ({}), {}", result.getResult(), e);
                    throw new RuntimeException(e);
                }
            }
        }

        return result;
    }

    public static void setupDB(Connection conn) {
        List<CommandDB> setupCommands = new ArrayList<>(10);
        if (existsTable(conn, "SUSERS")) {
            setupCommands.add(new DropTable("SUSERS"));
        }
        setupCommands.add(new RunSQL( "CREATE TABLE SUSERS(id INT PRIMARY KEY, guid VARCHAR(255), name VARCHAR(255))"));
        setupCommands.forEach(c -> c.execute(conn));
    }

    private static boolean existsTable(Connection conn, String tableName) {
        DatabaseMetaData meta = null;
        ResultSet rs = null;
        try {
            meta = conn.getMetaData();
            rs = meta.getTables(null, null, tableName, new String[] {"TABLE"});
            return rs.next();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    @Builder
    @Data
    public static class DBUtilConfig {
        enum Mode {
            CUSTOM, // IF STATEMENT
            DEFAULT; // DO NOTHING

            private static boolean autoCommit = false;
            private static int transactionIsolation = Connection.TRANSACTION_SERIALIZABLE;

            public static void configure(Connection conn) {
                if (config.mode == DBUtilConfig.Mode.CUSTOM) {
                    try {
                        conn.setAutoCommit(autoCommit);
                        conn.setTransactionIsolation(transactionIsolation);
                    } catch (SQLException e) {
                        log.error("Wrong db util configuration.");
                        System.exit(-14);
                    }
                }
            }
        }

        private Mode mode = Mode.DEFAULT;

    }
}
