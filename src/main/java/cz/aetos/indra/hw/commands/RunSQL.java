package cz.aetos.indra.hw.commands;

import cz.aetos.indra.hw.commands.util.CommandDB;
import cz.aetos.indra.hw.commands.util.CommandDBResult;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;

@Slf4j
public class RunSQL extends CommandDB {

    public RunSQL(String sql) {
        super(sql);
    }

    @Override
    public CommandDBResult execute(Connection conn) {
        log.info("start ( {} )", sqlTemplate);
        CommandDBResult result = executeInternal(conn, true, sqlTemplate);
        log.info("end");
        return result;
    }

}
