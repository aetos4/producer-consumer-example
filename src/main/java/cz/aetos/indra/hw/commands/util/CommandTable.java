package cz.aetos.indra.hw.commands.util;

public abstract class CommandTable extends CommandDB {

    protected final String tableName;

    protected CommandTable(String tableName, String sqlTemplate) {
        super(sqlTemplate);
        this.tableName = tableName;
    }

}
