package cz.aetos.indra.hw;

import cz.aetos.indra.hw.commands.util.CommandDB;
import cz.aetos.indra.hw.commands.util.CommandDBResult;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.util.concurrent.ArrayBlockingQueue;

@Slf4j
public class Consumer implements Runnable {

    private final ArrayBlockingQueue<CommandDB> queue;
    private final Connection conn;

    public Consumer(ArrayBlockingQueue<CommandDB> queue, Connection conn) {
        this.queue = queue;
        this.conn = conn;
    }


    @Override
    public void run() {
        log.info("Consumer start");

        boolean run = true;
        int counter = 0; // NOTE: this value is only for logging purpose
        while(run) {
            try {
                log.info("TAKE (or waiting for item from queue)");
                CommandDB dbCommand = queue.take();
                log.info("GOING TO EXECUTE " + dbCommand.getClass());
                CommandDBResult result = dbCommand.execute(conn);
                // end of work when result is set to 14 in some command
                if (result.getResult() != null && result.getResult() == 14) {
                    run = false;
                }
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            log.info("Counter: " + ++counter);
        }

        log.info("Consumer stop");
    }

}
