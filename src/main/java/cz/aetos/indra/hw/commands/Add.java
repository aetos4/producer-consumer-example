package cz.aetos.indra.hw.commands;

import cz.aetos.indra.hw.UserDTO;
import cz.aetos.indra.hw.commands.util.CommandDB;
import cz.aetos.indra.hw.commands.util.CommandDBResult;
import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.SQLException;

@Slf4j
public class Add extends CommandDB {
    private final UserDTO user;

    public Add(UserDTO user) {
        super("INSERT INTO SUSERS VALUES ($ID, '$GUID', '$NAME')");
        this.user = user;
    }

    @Override
    public CommandDBResult execute(Connection conn) {
        log.info("start ( {} )", sqlTemplate);
        String sql = sqlTemplate
                .replace("$ID", user.id().toString())
                .replace("$GUID", user.guid().toString())
                .replace("$NAME", user.name().toString());

        CommandDBResult result = executeInternal(conn, true, sql);

        log.info("end with result {}", result.getResult());
        return result;
    }


}
