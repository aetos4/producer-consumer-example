package cz.aetos.indra.hw.commands.util;

import cz.aetos.indra.hw.DBUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.derby.impl.jdbc.EmbedResultSet;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@Slf4j
public abstract class CommandDB {
    protected final String sqlTemplate;

    protected CommandDB() {
        sqlTemplate = null;
    }

    protected CommandDB(String sqlTemplate) {
        this.sqlTemplate = sqlTemplate;
    }

    public abstract CommandDBResult execute(Connection conn);

    protected synchronized CommandDBResult executeInternal(Connection conn, boolean update, String sql) {
        CommandDBResult result = DBUtil.executeSql(conn, update, sql);
        return result;
    }
}
