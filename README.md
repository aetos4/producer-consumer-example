# producer-consumer-example

Producer Consumer example was developed as homework for job interview.

## How to run or test

There are two scripts
* start.sh - package and run app
* test.sh - start 2 tests

requirements are:
* java 16 (records are used) 
* installed maven > 3.6.x.

For running of all we can type command
````bash
sh start.sh  && sh test.sh
````

## Demonstration

In order, we can evaluate the assignment, we just start the start.sh script and look at the std output.
In the Main.java class, prepare the following list as a batch for processing by a  consumer - takes all commands from the batch inserts them into a queue and finishes its job.

On the other side, is there a consumer, that processes each command from the queue?

In the solution is used only for 1 producer and 1 consumer, but the solution is prepared for many producers and consumers.

````java
private static List<CommandDB> batch = List.of(
        new Add(new UserDTO(1, "a1", "Robert")),
        new Add(new UserDTO(2, "a2", "Martin")),
        new PrintAll(),
        new DeleteAll(),
        new PrintAll(),
        new Exit()
);
````

## Issues

1. javadoc - javadoc is missing; only in the complicated parts of code are inline comments for better understanding.


----


## origin mission

    Create program in Java language that will process commands 
    from FIFO queue using Producer – Consumer pattern.
    
    Supported commands are the following:
    
    Add - adds a user into a database
    PrintAll – prints all users into standard output
    DeleteAll – deletes all users from database

    User is defined as database table SUSERS with columns (USER_ID, USER_GUID, USER_NAME)

    Demonstrate program on the following sequence (using main method or test):

    Add (1, "a1", "Robert")
    Add (2, "a2", "Martin")
    PrintAll
    DeleteAll
    PrintAll
    
    Show your ability to unit test code on at least one class.
    
    Goal of this exercise is to show Java language and JDK know-how, OOP principles, clean code
    understanding, concurrent programming knowledge, unit testing experience.
    
    Please do not use Spring framework in this exercise. Embedded database is sufficient.
